<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class IndexController extends FOSRestController
{
    /**
     * API homepage
     *
     * @ApiDoc(
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @FOSRest\Get(path="/")
     * @FOSRest\View(statusCode=200)
     *
     * @return array
     */
    public function getIndexAction()
    {
        return [
            'data' => [],
        ];
    }
}
