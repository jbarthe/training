<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Message;
use AppBundle\Form\MessageType;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MessageController extends FOSRestController
{
    /**
     * List all messages.
     *
     * @ApiDoc(
     *   resource = true,
     *   resourceDescription = "Operations on messages.",
     *   description = "Retrieve list of messages.",
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @Annotations\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing notes.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default="5", description="How many notes to return.")
     *
     * @Annotations\View(
     *   statusCode = Response::HTTP_OK
     * )
     *
     * @param Request               $request      the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */
    public function getMessagesAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:Message');
        $messages = $repository->findAll();

        return [
            'messages' => $messages,
        ];
    }

    /**
     * Creates a message.
     *
     * @ApiDoc(
     *   resource = true,
     *   input = "AppBundle\Form\MessageType",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     * @Annotations\View(
     *   statusCode = Response::HTTP_CREATED
     * )
     *
     * @param Request $request
     *
     * @return Form|[]
     */
    public function postMessagesAction(Request $request)
    {
        $message = new Message();
        $form = $this->createForm(MessageType::class, $message, [
            'method' => 'POST',
        ]);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($message);
            $em->flush();

            return [
                'message' => $message,
            ];
        }

        return $form;
    }
}
