<?php

namespace AppBundle\Serializer\Normalizer;

use AppBundle\Entity\Message;
use AppBundle\Entity\Tag;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\SerializerAwareNormalizer;

class MessageNormalizer extends SerializerAwareNormalizer implements NormalizerInterface
{
    /**
     * @param Message     $object
     * @param string|null $format
     * @param array       $context
     *
     * @return array
     */
    public function normalize($object, $format = null, array $context = array())
    {
        return [
            'id' => $object->getId(),
            'name' => $object->getText(),
            'category' => $this->serializer->normalize($object->getCategory(), $format, $context),
//            'tags' => array_map(
//                function (Tag $tag) {
//                    return $tag->getId();
//                },
//                $object->getTags()->toArray()
//            ),
            'tags' => array_map(
                function (Tag $tag) use ($format, $context) {
                    return $this->serializer->normalize($tag, $format, $context);
                },
                $object->getTags()->toArray()
            ),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof Message;
    }
}
