init: ## Start Docker containers
	docker-compose up -d

install: ## Install Composer dependencies
	docker-compose run -u www-data php composer install

ssh: ## SSH into the Symfony Docker container
	docker-compose run -u www-data php bash

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: help
