Feature: Messages

    Scenario: List messages
        Given I am authenticated as default user
        When I send a GET request to "/messages"
        Then the response should be in JSON
        And the JSON node "messages" should be equal to :text

    Scenario:
        Given I have some messages available
        When I try to list messages
        Then I should see some messages
