<?php

namespace Context;

use Behat\Behat\Context\Context;
use Sanpi\Behatch\HttpCall\Request;
use Sanpi\Behatch\Json\Json;

class DefaultContext implements Context
{
    /**
     * @var Request
     */
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @Given I am authenticated as default user
     */
    public function iAmAuthenticatedAsDefaultUser()
    {
        $this->request->send('POST', '/jwt/token', [
            'username' => 'user',
            'password' => 'user',
        ]);

        $json = new Json($this->request->getContent());

        $this->request->setHttpHeader(
            'Authorization',
            sprintf('Bearer %s', $json->getContent()->token)
        );
    }
}
