Symfony API & Elasticsearch training
====================================

Installation
------------

1. Démarrer les containers Docker via  `make init` ou via `docker-compose up -d`
2. Installation des vendors via `make install` ou via `composer install`

Si les 2 dernières commandes fonctionnent vous devriez pouvoir tester si tout 
fonctionne via :

* Exécuter `make ssh` et pouvoir lancer `./bin/console` ;
* Ouvrir `http://localhost:8080/` et voir `Welcome to Symfony 3.0.3`.

-------------

1. 
2. Bootstrap des bundles essentiels
3. Création des entités et listing des messages
4. Création d'un message

{
    "text": "Test Message",
    "category": {
        "name": "Test Category"
    },
    "tags": [
        {"name": "First tag"}
    ]
}

TODO:

- FOSElasticaBundle
- Behat : features API
- FOSElasticaBundle: custom provider
- FOSElasticaBundle: query listing
